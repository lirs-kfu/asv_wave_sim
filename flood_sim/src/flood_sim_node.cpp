// C++
#include <cmath>

// ROS
#include "ros/ros.h"

// Gazebo
#include "gazebo_msgs/SetModelState.h"
#include "gazebo_msgs/GetModelState.h"

constexpr double pi = std::acos(-1);

void uniform_wave_propagation(ros::ServiceClient &set_model_state_client, ros::ServiceClient &get_model_state_client,
                                    double height, double speed)
{

    double time = height / speed; // second

    double start_time = 0.0; // second

    double start_height = 0.0; // meter

    ros::Duration loop_dur(0.1); // .1 second

    // position
    geometry_msgs::Point wave_position;
    wave_position.x = 0.0;
    wave_position.y = 0.0;
    wave_position.z = 0.0;

    // orientation
    geometry_msgs::Quaternion wave_orientation;
    wave_orientation.x = 0.0;
    wave_orientation.y = 0.0;
    wave_orientation.z = 0.0;
    wave_orientation.w = 0.0;

    // pose
    geometry_msgs::Pose wave_pose;

    // model state
    gazebo_msgs::ModelState wave_modelstate;
    wave_modelstate.model_name = "ocean_waves";

    // model set state service
    gazebo_msgs::SetModelState model_state_srv;

    // model get state service
    gazebo_msgs::GetModelState get_model_state_srv;

    get_model_state_srv.request.model_name = "ocean_waves";

    double ocean_waves_height = 0.0;
    if (get_model_state_client.call(get_model_state_srv)) {
        ROS_INFO("[+] Wave height = %f", get_model_state_srv.response.pose.position.z);
        ocean_waves_height = get_model_state_srv.response.pose.position.z;
    }

    else
        ROS_ERROR("[-] Failed to get ocean_waves pose! Error msg: %s", get_model_state_srv.response.status_message.c_str());

    start_height = ocean_waves_height;

    while (ros::ok() && start_time < time) {

        wave_position.x = 0.0;
        wave_position.y = 0.0;
        wave_position.z = start_height;
        
        wave_pose.position = wave_position;
        wave_pose.orientation = wave_orientation;
        
        wave_modelstate.pose = wave_pose;
        
        model_state_srv.request.model_state = wave_modelstate;

        if (set_model_state_client.call(model_state_srv))
            ROS_INFO("[+] Wave move to height %f", wave_position.z);

        else
            //ROS_ERROR("[-] Failed to move magic waves! Error msg: %s", model_state_srv.response.status_message.c_str());        

        // Sleep 1 second
        loop_dur.sleep();

        start_time += 0.1;

        start_height = speed * start_time;

        ros::spinOnce();
    }

    ROS_INFO("[!] Uniform Flood wave propagation finished!");
}

void sinusoidal_wave_propagation(ros::ServiceClient &set_model_state_client, ros::ServiceClient &get_model_state_client,
                                    double wave_length, double amplitude)
{
    int i = 0;
    ros::Rate loop_rate(10);

    // model get state service
    gazebo_msgs::GetModelState get_model_state_srv;

    get_model_state_srv.request.model_name = "ocean_waves";

    double ocean_waves_height = 0.0;
    if (get_model_state_client.call(get_model_state_srv)) {
        ROS_INFO("[+] Wave height %f", get_model_state_srv.response.pose.position.z);
        ocean_waves_height = get_model_state_srv.response.pose.position.z;
    }

    else
        ROS_ERROR("[-] Failed to get ocean_waves pose! Error msg: %s", get_model_state_srv.response.status_message.c_str());

    while (ros::ok()) {
        
        ++i;

        geometry_msgs::Point wave_position;
        wave_position.x = 0.0;
        wave_position.y = 0.0;
        //wave_position.z = -2 + abs(4*sin(pi*i/600));
        wave_position.z = -5 + abs(amplitude*sin(pi*i/wave_length));
    
        geometry_msgs::Quaternion wave_orientation;
        wave_orientation.x = 0.0;
        wave_orientation.y = 0.0;
        wave_orientation.z = 0.0;
        wave_orientation.w = 0.0;

        geometry_msgs::Pose wave_pose;
        wave_pose.position = wave_position;
        wave_pose.orientation = wave_orientation;

        gazebo_msgs::ModelState wave_modelstate;
        wave_modelstate.model_name = "ocean_waves";
        wave_modelstate.pose = wave_pose;

        gazebo_msgs::SetModelState srv;
        srv.request.model_state = wave_modelstate;

        if (set_model_state_client.call(srv))
            ROS_INFO("Wave move to height %f", wave_position.z);

        else            
            ROS_ERROR("Failed to move magic waves! Error msg: %s", srv.response.status_message.c_str());

        // Sleep
        loop_rate.sleep();

        ros::spinOnce();
    }

    ROS_INFO("[!] Sinusoidal Flood wave propagation finished!");
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "flood");
    ros::NodeHandle node_handle("~");
    ros::ServiceClient set_model_state_client = node_handle.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
    set_model_state_client.waitForExistence();
    ros::ServiceClient get_model_state_client = node_handle.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state");
    get_model_state_client.waitForExistence();

    // parameters
    std::string wave_propagation_type = "";
    double height = 0.0, speed = 0.0, wave_length = 0.0, amplitude = 0.0;

    // Read parameters from launch file
    node_handle.param<std::string>("wave_propagation_type", wave_propagation_type, "uniform");
    node_handle.param<double>("height", height, 0.0);
    node_handle.param<double>("speed", speed, 0.0);
    node_handle.param<double>("wave_length", wave_length, 0.0);
    node_handle.param<double>("amplitude", amplitude, 0.0);

    // Show info
    ROS_INFO("[!] wave_propagation_type = %s\n", wave_propagation_type.c_str());
    ROS_INFO("[!] height = %f\n", height);
    ROS_INFO("[!] speed = %f\n", speed);
    ROS_INFO("[!] wave_length = %f\n", wave_length);
    ROS_INFO("[!] amplitude = %f\n", amplitude);

    if (wave_propagation_type == "uniform")
        uniform_wave_propagation(set_model_state_client, get_model_state_client, height, speed);
    else if (wave_propagation_type == "sinusoidal")
        sinusoidal_wave_propagation(set_model_state_client, get_model_state_client, wave_length, amplitude);
    else
        ROS_ERROR("[-] Unimplemented propagation type!");

    return 0;
}
